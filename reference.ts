const questionBox = document.getElementById('ipQue') as HTMLInputElement;
const dropdownBtn = document.getElementById('dropdown') as HTMLElement;
const checkboxBtn = document.getElementById('checkbox') as HTMLElement;
const numericBtn = document.getElementById('numeric') as HTMLElement;
const textBtn = document.getElementById('text') as HTMLElement;
const submitBtn = document.getElementById('submit') as HTMLElement;
const inputQuestionArea = document.getElementById('inputQuestionArea') as HTMLElement;

const outputQueArea = document.getElementById('outputQueArea') as HTMLElement;