let type = "";

const createDropdown = () => {
    type = 'dropdown';
    const dropdownBox = document.createElement('input');
    dropdownBox.setAttribute("input",'dropdown');
    inputQuestionArea.appendChild(dropdownBox);
    const addBtn = document.createElement('input');
    inputQuestionArea.appendChild(addBtn);
    addBtn.addEventListener('click', createDropdown);
}
dropdownBtn.addEventListener('click', createDropdown);

const createCheckBox = () => {
    const type = 'checkbox'
    const inputBox = document.createElement('input');
    inputBox.setAttribute("input",'checkbox');
    const addCheckBox = document.createElement('i');
    addCheckBox.innerText = '+';
    inputQuestionArea.appendChild(inputBox);
    addCheckBox.addEventListener('click',createCheckBox);
}
const createNumericBox = () => {
    const type = 'NumericText';
    const NumericInput = document.createElement('input');
    NumericInput.setAttribute("input", "text");
    const addNumericInputBox = document.createElement('i');
    addNumericInputBox.innerText = '+';
    inputQuestionArea.appendChild(NumericInput);
    addNumericInputBox.addEventListener('click',createNumericBox);
}
const createTextBox = () => {
    var type = 'Text';
    const textInput = document.createElement('input');
    textInput.setAttribute("input", "text");
    const addTextInputBox = document.createElement('i');
    addTextInputBox.innerText = '+';
    inputQuestionArea.appendChild(textInput);
    addTextInputBox.addEventListener('click',createTextBox);
}