var type = "";
var createDropdown = function () {
    type = 'dropdown';
    var dropdownBox = document.createElement('input');
    dropdownBox.setAttribute("input", 'dropdown');
    inputQuestionArea.appendChild(dropdownBox);
    var addBtn = document.createElement('input');
    inputQuestionArea.appendChild(addBtn);
    addBtn.addEventListener('click', createDropdown);
};
dropdownBtn.addEventListener('click', createDropdown);
var createCheckBox = function () {
    var type = 'checkbox';
    var inputBox = document.createElement('input');
    inputBox.setAttribute("input", 'checkbox');
    var addCheckBox = document.createElement('i');
    addCheckBox.innerText = '+';
    inputQuestionArea.appendChild(inputBox);
    addCheckBox.addEventListener('click', createCheckBox);
};
var createNumericBox = function () {
    var type = 'NumericText';
    var NumericInput = document.createElement('input');
    NumericInput.setAttribute("input", "text");
    var addNumericInputBox = document.createElement('i');
    addNumericInputBox.innerText = '+';
    inputQuestionArea.appendChild(NumericInput);
    addNumericInputBox.addEventListener('click', createNumericBox);
};
var createTextBox = function () {
    var type = 'Text';
    var textInput = document.createElement('input');
    textInput.setAttribute("input", "text");
    var addTextInputBox = document.createElement('i');
    addTextInputBox.innerText = '+';
    inputQuestionArea.appendChild(textInput);
    addTextInputBox.addEventListener('click', createTextBox);
};
